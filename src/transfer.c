/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <stdlib.h>
#include "setup.h"
#include "utils.h"
#include "progress.h"
#include "defs.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
unsigned int sum;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
unsigned int RevEndian (unsigned int a, int bigendian)
{
	if (bigendian)
	{
		unsigned int b = 0;
		b |= (a >> 24) & 0x000000FF;
		b |= (a >> 8) & 0x0000FF00;
		b |= (a << 8) & 0x00FF0000;
		b |= (a << 24) & 0xFF000000;
		return b;
	}
	else
		return a;
}

unsigned int CalcSum (unsigned char *buffer, int bigendian)
{
	unsigned int localsum = 0;

	for (int i = 0; i < SAVE_BLOCKSIZE/4; i++)
		localsum += RevEndian (((unsigned int*)buffer)[i], bigendian);

	return localsum;
}

int Transfer (int nbytes, FILE *from, int to, unsigned int blocki, int bigendian)
{
	// Block buffer, + checksum
	static unsigned char rdbuf[4];
	static unsigned char buffer[SAVE_BLOCKSIZE+4];
	static unsigned int localsum = 0;

	// Retransmit
	int retransmit = 0;

	// Wait for "ready"
	if (read (to, rdbuf, 1) != 1)
		return 1;
	else if (rdbuf[0] == 'e')
	{
		printf ("\nBlock error\n");
		MsSleep (1);
		if (read (to, rdbuf, 4) != 4)
			return 7;

		unsigned int blockno = ((unsigned int*)rdbuf)[0];
		printf ("\nRetransmit, transfer error, oldsum %.8X, mcu blockno %i, expected %i\n",
			localsum, blockno, blocki);

		retransmit = 1;
		MsSleep (50);
	}
	else if (rdbuf[0] == 'c')
	{
		printf ("\nChecksum error\n");
		MsSleep (1);
		if (read (to, rdbuf, 4) != 4)
			return 7;

		unsigned int blockno = ((unsigned int*)rdbuf)[0];
		printf ("\nRetransmit, transfer error, oldsum %.8X, mcu blockno %i, expected %i\n",
			localsum, blockno, blocki);

		retransmit = 1;
		MsSleep (50);
	}
	else if (rdbuf[0] == 'o')
	{
		// End of transfer
		return 6;
	}
	else if (rdbuf[0] != 'r')
	{
		printf ("\nGot 0x%X (%c) instead: %c", rdbuf[0], rdbuf[0], rdbuf[0]);
		fflush (stdout);

		while (BlockingRead (rdbuf, 1, to, 10) == 0)
		{
			printf ("%c", rdbuf[0]);
			fflush (stdout);
		}

		return 2;
	}

	// Read block of data from source
	if (!retransmit)
		if (fread (buffer, 1, nbytes, from) != nbytes)
			return 3;

	// Always calc sum
	localsum = CalcSum (buffer, bigendian);

	// Sum it
	if (!retransmit)
		sum += localsum;

	// Write sum
	((unsigned int*)(buffer+SAVE_BLOCKSIZE))[0] = RevEndian (localsum, bigendian);

	// Slow write
	if (SlowWrite (buffer, SAVE_BLOCKSIZE+4, to, 0))
		return 4;

	// Return 5 if loop should not increment
	if (retransmit)
		return 5;

	// Otherwise we're done with this block
	return 0;
}

void TransferMain (FILE *fp, int uart, int size, int speed, int startspeed, int bigendian)
{
	// Vars
	int ret = 0;

	// Calculate number of blocks and remainder
	int nblocks = size / SAVE_BLOCKSIZE;
	int nremain = size % SAVE_BLOCKSIZE;

	// Inform
	printf ("size %i nblocks %i nremain %i speed %i startspeed %i bigendian %i\n",
		(int)size, nblocks, nremain, speed, startspeed, bigendian);

	// Clear global checksum
	sum = 0;

	// Write blocks
	for (int i = 0; i < nblocks; i++)
	{
		do
		{
			ret = Transfer (SAVE_BLOCKSIZE, fp, uart, i, bigendian);
		}
		while (ret == 5);

		if (ret)
		{
			printf ("Transfer error %i\n", ret);
			return;
		}
		else
			PrintProgress (i+1, nblocks);
	}

	// Write remainder
	if (nremain)
	{
		do
		{
			if (ret = Transfer (nremain, fp, uart, 0xFFFFFFFF, bigendian))
			{
				printf ("Transfer error %i\n", ret);
				return;
			}
			else
				PrintProgress (nblocks, nblocks);
		} while (ret == 5);
	}

	// Wait for 'o'
	ret = Transfer (nremain, fp, uart, 0, bigendian);

	if (ret && ret != 6)
		printf ("Transfer error %i\n", ret);
	else
		PrintProgress (nblocks, nblocks);

	// IMPORTANT: Strange stuff happens if InitializePort happens too early!!!
	MsSleep (2);
	InitializePort (uart, SpeedToTermios(speed), 1);

	// Try reading that checksum
	if (ret == 6)
	{
		char buf[16];

		if ((ret = BlockingRead (buf, 8, uart, 1000)))
			printf ("Echo error, ret = %i\n", ret);
		else
		{
			buf[8] = 0;

			int recvsum = strtol (buf, 0, 16);

			if (sum == recvsum)
				printf ("\nChecksum OK\n");
			else
				printf ("\nChecksum mismatch: Got sum %.8X expected %.8X\n", recvsum, sum);
		}
	}
	else
		printf ("\n");
}

int main (int argc, char **argv)
{
	// Usage
	if (argc < 4 || argc > 7)
	{
		printf ("Usage: transfer <infile> <portname> <targetname>\n");
		printf ("Usage: transfer <infile> <portname> <targetname> <speed>\n");
		printf ("Usage: transfer <infile> <portname> <targetname> <speed> <startspeed>\n");
		printf ("Usage: transfer <infile> <portname> <targetname> <speed> <startspeed> <bigendian>\n");
		return 1;
	}

	// Speed
	int speed = 230400;
	if (argc >= 5)
		speed = atoi (argv[4]);

	// Open and check input file
	FILE *fp = fopen (argv[1], "rb");

	if (!fp)
	{
		printf ("Error opening %s\n", argv[1]);
		return 1;
	}

	// Open and check output file
	int uart = open (argv[2], O_RDWR | O_NOCTTY | O_SYNC);

	if (uart < 0)
	{
	    printf ("error %d opening %s: %s", errno, argv[2], strerror (errno));
	    if (fp)
			fclose (fp);
	    return 1;
	}

	// Initialize uart
	int startspeed = 115200;

	if (argc >= 6)
		startspeed = atoi (argv[5]);

	InitializePort (uart, SpeedToTermios(startspeed), 1);

	// Endianness setting
	int bigendian = 0;

	if (argc >= 7)
		bigendian = atoi (argv[6]);

	// Get size of input file
	fseek (fp, 0, SEEK_END);
	long size = ftell (fp);
	fseek (fp, 0, 0);

	// Vars
	char buf[64];
	int ret = 0;

	// Create command
	sprintf (buf, "save %s %i %i\r", argv[3], (int) size, speed);

	// Write command
	if (SlowWrite (buf, strlen (buf), uart, 5))
	{
		printf ("Write command error\n");
		return 1;
	}

	// Read the echo command
	if ((ret = BlockingRead (buf, strlen (buf), uart, 1000)))
	{
		printf ("Echo error, ret = %i\n", ret);
		return 1;
	}

	// Read the echo newline
	if (read (uart, buf, 1) != 1 || buf[0] != '\n')
	{
		printf ("Response not correct, buf[0] = 0x%.2x\n", buf[0]);
		return 1;
	}

	// Set uart speed
	MsSleep (100);
	InitializePort (uart, SpeedToTermios(speed), 1);
	MsSleep (100);

	// Transfer
	TransferMain (fp, uart, size, speed, startspeed, bigendian);

	// Close files
	fclose (fp);
	close (uart);
}
