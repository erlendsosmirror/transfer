/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <time.h>
#include "defs.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void PrintProgress (int i, int n)
{
	// Static values of old values
	static int nsymold = -1;
	static int oldsecs = 0;
	static int oldi = 0;
	static float speed = 0.0f;

	// Calculate the percentage and the number of symbols to show
	float percent = (float) i / (float) (n ? n : 1);
	float nsymbols = percent * 40.0f;

	// Number of symbols in int
	int nsym = (int) nsymbols;

	// Get time
	struct tm *tm;
    time_t now = time(0);
    tm = localtime (&now);

	// Update progress bar if the number of symbols changed or a second
	// elapsed.
	if (nsym != nsymold || tm->tm_sec != oldsecs)
	{
		// Also update speed once a second
		if (tm->tm_sec != oldsecs)
		{
			speed = (float)((i - oldi) * SAVE_BLOCKSIZE);
			oldi = i;
		}

		// Save old values
		nsymold = nsym;
		oldsecs = tm->tm_sec;

		// Print bar
		printf ("\r%3.0f%% [", percent * 100.0f);

		for (int j = 0; j < nsym; j++)
			printf ("=");

		for (int j = nsym; j < 40; j++)
			printf (" ");

		printf ("] block %i/%i (%i Bps)", i, n, (int)speed);
		fflush(stdout);
	}
}
