/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int MsSleep (unsigned int ms)
{
	int ret = 0;

	struct timespec remaining =
	{
		ms / 1000,
		(ms % 1000) * 1000000L
	};

	do
	{
		struct timespec time = remaining;
		ret = nanosleep(&time, &remaining);
	} while (ret == EINTR);

	if (ret)
		perror("nanosleep failed");

	return ret;
}

int SlowWrite (char *buf, int nbytes, int to, int time)
{
	if (time)
	{
		for (int i = 0; i < nbytes; i++)
		{
			if (write (to, &buf[i], 1) != 1)
				return 1;

			MsSleep (time);
		}
	}
	else if (write (to, buf, nbytes) != nbytes)
		return 1;

	return 0;
}

int BlockingRead (char *str, int count, int from, int timeout)
{
	int nbytes = 0;
	int time = 0;

	while (nbytes != count)
	{
		int nread = read (from, &str[nbytes], count - nbytes);

		if (nread < 0)
			return 2;

		nbytes += nread;

		if (nbytes != count)
		{
			time++;

			if (time >= timeout)
			{
				printf ("Timeout with %i bytes\n", nbytes);
				return 1;
			}
			else
				MsSleep (1);
		}
	}

	return 0;
}
