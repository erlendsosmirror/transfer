transfer: src/transfer.c src/setup.c src/utils.c src/progress.c
	@gcc -o transfer -I./inc src/transfer.c src/setup.c src/utils.c src/progress.c

clean:
	@rm -rf transfer
